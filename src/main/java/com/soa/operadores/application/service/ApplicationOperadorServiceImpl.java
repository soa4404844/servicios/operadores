package com.soa.operadores.application.service;



import com.soa.operadores.application.api.OperadorApiDelegate;
import com.soa.operadores.application.mapper.ApplicationOperadorMapper;
import com.soa.operadores.application.model.ObtenerOperador;
import com.soa.operadores.domain.model.Operador;
import com.soa.operadores.domain.service.DomainOperadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ApplicationOperadorServiceImpl implements OperadorApiDelegate {

    @Autowired
    private ApplicationOperadorMapper applicationOperadorMapper;

    @Autowired
    private DomainOperadorService domainOperadorService;

    @Override
    public ResponseEntity<List<ObtenerOperador>> consultarOperadores(String parametro, ObtenerOperador parametros) {
        var productos = domainOperadorService.getOperadores(
                parametro,
                applicationOperadorMapper.obtenerOperadorToOperador(parametros)
        );

        return ResponseEntity.ok(
                productos.stream()
                        .map(applicationOperadorMapper::operadorToObtenerOperador)
                        .toList()
        );
    }

    @Override
    public ResponseEntity<ObtenerOperador> obtenerOperador(Integer operadorId) {

        var operador = domainOperadorService.getOperador(Operador.builder()
                .operadorID(operadorId)
                .build()
        );

        return ResponseEntity.ok(
                applicationOperadorMapper.operadorToObtenerOperador(operador)
        );

    }
}

