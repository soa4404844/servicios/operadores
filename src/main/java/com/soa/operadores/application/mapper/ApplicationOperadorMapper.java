package com.soa.operadores.application.mapper;

import com.soa.operadores.application.model.ObtenerOperador;
import com.soa.operadores.domain.model.Operador;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ApplicationOperadorMapper {

    Operador obtenerOperadorToOperador(ObtenerOperador pbtenerOperador);

    ObtenerOperador operadorToObtenerOperador(Operador operador);



}
