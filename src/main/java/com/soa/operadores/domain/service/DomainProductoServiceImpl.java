package com.soa.operadores.domain.service;

import com.soa.operadores.domain.model.Operador;
import com.soa.operadores.domain.repository.DomainOperadorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DomainProductoServiceImpl implements DomainOperadorService {

    @Autowired
    private DomainOperadorRepository domainOperadorRepository;


    @Override
    public List<Operador> getOperadores(String parametro, Operador parametros) {
        return domainOperadorRepository.findByPorParametro(parametro, parametros);
    }

    @Override
    public Operador getOperador(Operador operador) {
        return domainOperadorRepository.findById(operador.getOperadorID())
                .orElse(Operador.builder().build());
    }

}
