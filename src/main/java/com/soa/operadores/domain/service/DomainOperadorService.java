package com.soa.operadores.domain.service;

import com.soa.operadores.domain.model.Operador;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DomainOperadorService {

    List<Operador> getOperadores(String parametro, Operador parametros);

    Operador getOperador(Operador operador);


}
