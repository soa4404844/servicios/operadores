package com.soa.operadores.domain.repository;

import com.soa.operadores.domain.model.Operador;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DomainOperadorRepository extends CrudRepository<Operador, Integer> {

    @Query("""
            SELECT O FROM OPERADOR O
            """)
    List<Operador> findByPorParametro(
            @Param("parametro") String parametro,
            @Param("operadorTabla") Operador operador
    );


}
